﻿using HomeWork41.Enum;
using HomeWork41.Interfeces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork41.Models
{
    public class Visits : IEntity<int>
    {
        public int Id { get; set; }
        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public VisitsType VisitsType{ get; set; }
        public DateTime DateVisit { get; set; }
        public StateVisit StateVisits { get; set; }
        public decimal Price { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
