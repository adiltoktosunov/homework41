﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork41
{
    public enum VisitsType
    {
        Orthoped = 1,
        Orthodon = 2,
        DentistSurgeon = 3,
        ChildrensDentist = 4
    }
}
