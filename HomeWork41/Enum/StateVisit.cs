﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork41.Enum
{
    public enum StateVisit
    {

       Visited = 1,
       Pending = 2,
       Transfer = 3
    }
}
